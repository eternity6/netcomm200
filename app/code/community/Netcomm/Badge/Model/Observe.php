<?php
class Netcomm_Badge_Model_Observe
{
        public function intercept_order(& $observer){
        	
        	$order = $observer->getEvent()->getOrder();

            $verify = array();

        	foreach ($order->getAllStatusHistory() as $orderComment) {
        		$verify[$orderComment->getStatus()]++; 
        	}

            if (($order->getStatus() == Mage::getStoreConfig('netcomm_global/sendorder/sendorder')) && ($verify[Mage::getStoreConfig('netcomm_global/sendorder/sendorder')] <= 1)) {
                $baseurl_store = Mage::app()->getStore($order->getStore_id())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

	        	// ********************************
				// Getting informations about order
				// and products
				$objproducts = $order->getAllItems();
				unset($fd_products);
				
				foreach ($objproducts as $itemId => $item) {
					unset($tmp);
                    if (!$item->getParentItem()) {
                        $fd_oProduct = Mage::getModel('catalog/product')->load((int) $item->getProductId());

                        if ($fd_oProduct->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
                            $selectionCollection = $fd_oProduct->getTypeInstance(true)->getSelectionsCollection(
                                $fd_oProduct->getTypeInstance(true)->getOptionsIds($fd_oProduct), $fd_oProduct
                            );
                            foreach($selectionCollection as $option) {
                                $bundleproduct = Mage::getModel('catalog/product')->load($option->product_id);

                                $tmp['Id'] = $bundleproduct->getProductId();

                                Mage::getModel('core/url_rewrite')->loadByRequestPath(
                                    $tmp['Url'] = Mage::app()->getStore($order->getStoreId())->getUrl($bundleproduct->getUrlPath())
                                );
								
                                if ($fd_oProduct->getImage() != "no_selection")
                                    $tmp['ImageUrl'] = Mage::getModel('catalog/product_media_config')->getMediaUrl( $bundleproduct->getImage() );
                                else
                                    $tmp['ImageUrl'] = "";
                                //$tmp['sku'] = $item->getSku();

                                $tmp['Name'] = $bundleproduct->getName();
                                $tmp['Brand'] = $bundleproduct->getBrand();
                                if (is_null($tmp['Brand'])) $bundleproduct['Brand']  = "";
                                $fd_products[] = $tmp;
                            }
                        } else {


                            /* controllo se il prodotto è figlio di un prodotto raggrupato */
                            if (!empty(Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($item->getProductId())))
                            {
                                Mage::log('Identificato prodotto Configurabile.', null, 'netcomm200.log', true);
                                /* carico il padre */
                                $ay_id = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($item->getProductId());
                                Mage::log('Identificati i seguenti ID padri: '.Zend_Debug::dump($ay_id, null, false), null, 'netcomm200.log', true);
                                $tmp['Id'] = $ay_id[0];
                                $fd_oProduct = Mage::getModel('catalog/product')->load((int) $tmp['Id']);
                            
                            } 
                            
                            elseif (!empty(Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($item->getProductId())))
                            {
                                Mage::log('Identificato prodotto Raggrupato', null, 'netcomm200.log', true);
                                /* carico il padre */
                                $ay_id = Mage::getModel('catalog/product_type_grouped')->getParentIdsByChild($item->getProductId());
                                Mage::log('Identificati i seguenti ID padri: '.Zend_Debug::dump($ay_id, null, false), null, 'netcomm200.log', true);
                                $tmp['Id'] = $ay_id[0];
                                $fd_oProduct = Mage::getModel('catalog/product')->load((int) $tmp['Id']);
                            
                            } 
                            
                            else {
                                $tmp['Id'] = $item->getProductId();
                            }

                            Mage::getModel('core/url_rewrite')->loadByRequestPath(
                                $tmp['Url'] = Mage::app()->getStore($order->getStoreId())->getUrl($fd_oProduct->getUrlPath())
                            );
							
                            if ($fd_oProduct->getImage() != "no_selection")
                                $tmp['ImageUrl'] = Mage::getModel('catalog/product_media_config')->getMediaUrl( $fd_oProduct->getImage() );
                            else
                                $tmp['ImageUrl'] = "";
                            //$tmp['sku'] = $item->getSku();

                            $tmp['Name'] = $item->getName();
                            $tmp['Brand'] = $item->getBrand();
                            if (is_null($tmp['Brand'])) $tmp['Brand']  = "";


                            //$tmp['Price'] = $item->getPrice();
                            $fd_products[] = $tmp;
                        }


                    }
				}
				// ********************************
				
				// *******************************
				// Formatting the array to be sent
				$tmp_order['OrderId'] = $order->getId();
				$tmp_order['OrderDate'] = date("Y-m-d H:i:s");
				$tmp_order['CustomerEmail'] = $order->getCustomerEmail();
				$tmp_order['CustomerId'] = $order->getCustomerEmail();
				$tmp_order['Platform'] = "Magento ".MAGE::getVersion();
				$tmp_order['Products'] = $fd_products;

				$fd_data['merchantCode'] = Mage::getStoreConfig('netcomm_global/netcomm_preferences/netcomm_code');
                $fd_data['orders'][] = $tmp_order;

				// *******************************
				
				// *********************************
                // Sending request to feedaty server
                Mage::log('Inviate le seguenti informazioni: '.Zend_Debug::dump($fd_data, null, false), null, 'netcomm200.log', true);
				Netcomm_Badge_Model_WebService::send_order($fd_data);
				// *********************************
				
        	}
        }
}